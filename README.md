# Sonalake selenium challenge

## Requirements
Java: recommended 14 (developed on openJdk 14) / 9 should be enough (used `var`'s)   
Maven 3+ (3.6.3 recommended) - to run tests and builds   
Lombok plugin - with enabled annotation processing

## How to run
in command line in root of project `mvn clean install`  OR  
in IDE run class `CalculatorTest`

#### Optional parameters
To launch tests on different browser than Chrome you can use paremeter: `-Dselenide.browser=FIREFOX`  
Possible values are: `EDGE`,`FIREFOX`,`CHROME`  
Normally it could be a part of config that could be overridden by argument

## Info
Project uses Selenide (wrapper for selenium) because it is easier to use and much faster. 
It allows to skip much of start-up configuration, and also it includes `bonigarcia/webdrivermenager` 
which allows us to skip manual download of drivers for browsers  - reducing boilerplate code is great :)  
fun fact: In test automation template that I develop with my friend I use spring context for configuration
management, but I have found that it could be a little overkill for this particular challenge :sweat_smile:   

Probably I could delombok project because it is used only for @Slf4j annotation. But it probably would get more
valuable as project would growth.  

If it was not just a recruitment test but a true tests of calculator, then it should contain not only 
setValue() with given equation, but also variant where data would be inserted via buttons (probably it should be 
separate test set for just button inputs). 
I just wanted to save  some of my time. If it was intention to do it this way 
(via buttons not input field) I can adapt or write test set for this.