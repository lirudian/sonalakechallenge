package pl.durilian.sonalakechallange.components;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selenide.$;

public class CookiesPopup {
    private final static By CLOSE_COOKIES_BUTTON = byCssSelector("[aria-label='Consent']");
    private final static By POPUP = byCssSelector(".fc-dialog-container");

    public void closeCookiesPopup() {
        if (isDisplayed()) {
            $(CLOSE_COOKIES_BUTTON).click();
            $(POPUP).should(Condition.disappear);
        }
    }

    private boolean isDisplayed() {
        return $(POPUP).isDisplayed();
    }
}
