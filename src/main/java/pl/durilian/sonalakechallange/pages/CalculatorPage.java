package pl.durilian.sonalakechallange.pages;

import com.codeborne.selenide.Selenide;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import pl.durilian.sonalakechallange.components.CookiesPopup;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byCssSelector;
import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.confirm;
import static pl.durilian.sonalakechallange.utils.ConditionExtension.newResult;

@Slf4j
public class CalculatorPage {
    //I prefer to keep locators as By in opposite to SelenideElements as in some situations it may occurs in staleElementException
    //And in opposite to elements they are can be truly static final :)
    private static final By INPUT = byCssSelector("[name='input']");
    private static final By HISTORY_RESULT = byCssSelector("p[class='r']");
    private static final By SHOW_HISTORY = byCssSelector(".btn.dropdown-toggle.pull-right");
    private static final By HISTORY_FRAME = byId("histframe");
    private static final By LIST = byCssSelector("li");
    private static final By RESULT = byId("result");
    private static final By RAD_RADIO = byId("trigorad");
    private static final By CLEAR_HISTORY = byId("clearhistory");
    private static final CookiesPopup popup = new CookiesPopup();

    public CalculatorPage() {
        String url = "http://web2.0calc.com/";
        log.info("Opening website: {}", url);
        Selenide.open(url);
        popup.closeCookiesPopup();
    }

    public CalculatorPage calculateEquation(String equation) {
        log.info("Calculating equation: {}", equation);
        $(INPUT)
                .setValue(equation)
                .pressEnter();
        waitForResult();
        return this;
    }

    /***
     * This method could be solver on many ways.
     * Especialy due to fact that currently only history elements
     * contains attribute [data-inp] so we could find it directly
     * I decided it is safer to narrow elements to li elements of HISTORY_FRAME
     * @return List of equations as List<String>
     */
    public List<String> getCalculationsHistory() {
        log.info("Collecting calculations history...");
        revealHistory();
        var result = $(HISTORY_FRAME).$$(LIST)
                .stream()
                .map(elem -> elem.$("[data-inp]").getAttribute("data-inp"))
                .collect(Collectors.toList());
        log.info("Collected: {} calculations from history", result.size());

        return result;
    }

    public CalculatorPage clearHistory() {
        revealHistory();
        $(CLEAR_HISTORY).click();
        confirm();
        return this;
    }

    public String getHistoryResult() {
        log.info("Collecting result from history...");
        var result = $(HISTORY_RESULT).getAttribute("title");
        log.info("Collected result from history: {}", result);
        return result;
    }

    public String getResult() {
        log.info("Collecting result...");
        var result = $(INPUT).getValue();
        log.info("Collected result: {}", result);
        return result;
    }

    public CalculatorPage selectRad() {
        log.info("Selecting Rad option...");
        $(RAD_RADIO).click();
        log.info("Selected Rad option");
        return this;
    }

    private void revealHistory() {
        if (!$(HISTORY_FRAME).isDisplayed()) {
            $(SHOW_HISTORY).click();
            $(HISTORY_FRAME).shouldBe(visible);
        }
    }

    private void waitForResult() {
        log.info("Waiting for result...");
        $(RESULT).shouldHave(newResult());
    }
}
