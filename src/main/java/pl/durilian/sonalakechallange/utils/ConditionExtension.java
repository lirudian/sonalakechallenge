package pl.durilian.sonalakechallange.utils;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Driver;
import org.openqa.selenium.WebElement;

import javax.annotation.ParametersAreNonnullByDefault;

public class ConditionExtension {
    public static Condition newResult() {
        return new Condition("new result") {
            @Override
            @ParametersAreNonnullByDefault
            public boolean apply(Driver driver, WebElement webElement) {
                return webElement.getAttribute("style").equalsIgnoreCase("display: inline;");
            }
        };
    }
}
