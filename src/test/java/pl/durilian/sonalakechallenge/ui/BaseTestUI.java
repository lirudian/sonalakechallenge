package pl.durilian.sonalakechallenge.ui;

import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.AfterEach;

public class BaseTestUI {

    @AfterEach
    /***
     * I have found that history was kept between tests and might have interfered the results
     */
    void tearDown() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }
}
