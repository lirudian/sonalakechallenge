package pl.durilian.sonalakechallenge.ui;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import pl.durilian.sonalakechallange.pages.CalculatorPage;

@ExtendWith(SoftAssertionsExtension.class)
public class CalculatorTest extends BaseTestUI {
    CalculatorPage calculator;

    @BeforeEach
    void setup() {
        calculator = new CalculatorPage();
    }

    @Test
    void e2eCalculatorTest(SoftAssertions softly) {
        String calc1 = "35*999+(100/4)";
        String calc2 = "cos(pi)";
        String calc3 = "sqrt(81)";

        String expectedResult1 = "34990";
        String expectedResult2 = "-1";
        String expectedResult3 = "9";

        var result1 = calculator.calculateEquation(calc1).getResult();
        var historyResult1 = calculator.getHistoryResult();
        softly.assertThat(result1).isEqualTo(expectedResult1);
        softly.assertThat(historyResult1).isEqualTo(expectedResult1);

        calculator.selectRad();
        var result2 = calculator.calculateEquation(calc2).getResult();
        var historyResult2 = calculator.getHistoryResult();
        softly.assertThat(result2).isEqualTo(expectedResult2);
        softly.assertThat(historyResult2).isEqualTo(expectedResult2);

        var result3 = calculator.calculateEquation(calc3).getResult();
        var historyResult3 = calculator.getHistoryResult();
        softly.assertThat(result3).isEqualTo(expectedResult3);
        softly.assertThat(historyResult3).isEqualTo(expectedResult3);

        var history = calculator.getCalculationsHistory();
        softly.assertThat(history).hasSize(3);
        softly.assertThat(history).containsExactly(calc3, calc2, calc1);
    }

    @Test
    /***
     * I have found this challenge quite entertaining, so I decided to write small bonus test :)
     */
    void clearHistoryTest(SoftAssertions softly) {
        String calc1 = "1+1";
        String calc2 = "(3+3)*3";

        calculator.calculateEquation(calc1);
        calculator.calculateEquation(calc2);

        var listBeforeCleaning = calculator.getCalculationsHistory();
        var listAfterCleaning = calculator.clearHistory().getCalculationsHistory();

        softly.assertThat(listBeforeCleaning).hasSize(2);
        softly.assertThat(listAfterCleaning).isEmpty();
    }
}
